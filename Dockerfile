FROM ruby:2.6.6-alpine

ENV BUNDLER_VERSION=2.1.4

RUN echo 'gem: --no-document' > .gemrc

RUN apk add --update \
    gcc \
    bash \
    yarn \
    nodejs \
    musl-dev \
    build-base \
    libxslt-dev \
    make \
    cmake \
    abuild \
    tzdata \
    binutils \
    libxml2-dev \
    postgresql-dev

RUN gem update --system && gem install bundler:$BUNDLER_VERSION --no-document
